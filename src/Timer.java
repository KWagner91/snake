import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer {
    private Model model;
    private Graphics graphics;
    private long currentTime = System.nanoTime();
    public static boolean endScreen = false;


    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }

    @Override
    public void handle(long nowNano) {
        if (nowNano - currentTime > 1000 ) {
            model.update();
            graphics.draw();
            currentTime = nowNano;
            Main.score.setText("Score: " + model.score);

            if (model.isDead()) {
                System.out.println("Your final score: " + model.score);
                System.out.print("GAME OVER");
                this.endScreen = true;
                model.update();
                graphics.draw();
                stop();
            }
        }

    }
}
