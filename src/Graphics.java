import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import model.Block;
import model.Food;
import model.Model;
import model.Snake;

public class Graphics {
    private Model model;
    private GraphicsContext gc;


    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        // Snake
        gc.setFill(Color.DARKGREEN);
        Snake snake = model.getSnake();
        for (Block block : snake.getBlocks()) {
            gc.fillRect(
                    block.getPosX(),
                    block.getPosY(),
                    Block.blockSize,
                    Block.blockSize
            );
            block.setTranslateX(block.getPosX() * block.blockSize);
            block.setTranslateY(block.getPosY() * block.blockSize);
        }
        // Head
        gc.setFill(Color.FORESTGREEN);
        gc.fillRect(Snake.head.getPosX(), Snake.head.getPosY(), Block.blockSize, Block.blockSize);

        // Food
        Food food = model.getFood();
        gc.setFill(new LinearGradient(0, 0, 1, 2, true,
                CycleMethod.REPEAT, new Stop[]{new Stop(0, Color.YELLOW), new Stop(0.5f, Color.DARKRED)}));
        gc.fillOval(food.getPosX(), food.getPosY(), Block.blockSize, Block.blockSize);
        food.setTranslateX(food.getPosX() * Block.blockSize);
        food.setTranslateY(food.getPosY() * Block.blockSize);

        // Textfield at the end of game
        if (Timer.endScreen) {
            DropShadow ds = new DropShadow();
            ds.setOffsetY(3.0f);
            ds.setColor(Color.color(0.3f, 0.3f, 0.3f));

            gc.setFill(Color.RED);
            gc.setEffect(ds);
            gc.setFont(new Font("Arial", 100));
            gc.fillText("You lost!", Model.WIDTH/4, Model.HEIGHT/2);
        }
    }
    }

