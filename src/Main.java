import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.Model;


public class Main extends Application {

    public static Label score;
    private Timer timer;

    @Override
    public void start(Stage stage) throws Exception {

        Model model = new Model();

        VBox root = new VBox(10);
        root.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));

        Canvas canvas = new Canvas(model.WIDTH, model.HEIGHT);

        root.getChildren().add(canvas);

        score = new Label("Score: ");
        score.setFont(new Font("Arial", 22));
        root.getChildren().add(score);
        root.setBackground(new Background(new BackgroundFill(Color.OLDLACE, CornerRadii.EMPTY, Insets.EMPTY)));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Snake Game");
        stage.show();

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Graphics graphics = new Graphics(model, gc);
        timer = new Timer(model, graphics);
        timer.start();

        InputHandler inputHandler = new InputHandler(model);

        scene.setOnKeyPressed(event ->
                inputHandler.onKeyPressed(event.getCode()));
            }

    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }


}
