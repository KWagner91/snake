import javafx.scene.input.KeyCode;
import model.Block;
import model.Model;
import model.Snake;


public class InputHandler {
    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.UP && Snake.getDirection() != Block.DOWN) {
            Snake.changeDirection(Block.UP);
        } else if (key == KeyCode.DOWN && Snake.getDirection() != Block.UP ) {
            Snake.changeDirection(Block.DOWN);
        } else if (key == KeyCode.LEFT && Snake.getDirection() != Block.RIGHT) {
            Snake.changeDirection(Block.LEFT);
        } else if (key == KeyCode.RIGHT && Snake.getDirection() != Block.LEFT) {
            Snake.changeDirection(Block.RIGHT);
        }
    }

}
