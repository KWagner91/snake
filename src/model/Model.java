package model;

import java.util.Random;

public class Model {
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;
    private Snake snake;
    private Food food;
    public int score;

    public Snake getSnake() {
        return this.snake;
    }

    public Food getFood() {
        return this.food;
    }

    public Model() {
        snake = new Snake(100);
        update();
        addFood();
    }

    private void addBlock(Block b) {
        snake.getBlocks().add(b);
    }

    public void update() {
        for (Block b : snake.getBlocks()) {
            b.update();
        }
        if (hasEaten(food)) {
            score += 25;
            food = null;
            addFood();
            addNewBlock();
        }
    }

    // 10px around game board are forbidden for food to appear
    public void addFood() {
        Random random = new Random();

        int n = random.nextInt(WIDTH - 20) + 10;
        int randomX = n;

        int m = random.nextInt(HEIGHT - 20) + 10;
        int randomY = m;
        this.food = new Food(randomX, randomY);
    }

    public boolean hasEaten(Food food) {
        if (food == null) {
            return false;
        }
        // Snake and Food position is only 1 field on PosX and PosY -> Increase radius around food +/- 10
        return (snake.head.getPosX() >= food.getPosX() - 10 &&
                snake.head.getPosX() <= food.getPosX() + 10) &&
                (snake.head.getPosY() >= food.getPosY() - 10 &&
                        snake.head.getPosY() <= food.getPosY() + 10);
    }

    public void addNewBlock() {
        for (int i = 0; i < 7; i++) {
        Block b = new Block(snake.tail.getOldPosX(), snake.tail.getOldPosY(), snake.tail);
        addBlock(b);
        snake.tail = b;
        }
    }

    public boolean isDead() {
        for (Block b : snake.getBlocks()) {
            if (b != snake.head) {
                if (snake.head.getPosX() == b.getPosX() &&
                        (snake.head.getPosY() == b.getPosY())) {
                    return true;
                }
            }
        }
        return false;
    }

}


