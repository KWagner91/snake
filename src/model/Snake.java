package model;

import java.util.ArrayList;
import java.util.List;

public class Snake {

    private List<Block> blocks = new ArrayList<>();
    public static Block head;
    public Block tail;

    public List<Block> getBlocks() {
        return blocks;
    }

    public Snake(int initialLength) {
        int initialX = Model.WIDTH / 2;
        int initialY = Model.HEIGHT / 2;

        head = new Block(initialX, initialY, null);
        blocks.add(head);
        tail = head;

        // Each block becomes tail after being added to snake
        for (int i = 1; i < initialLength; i++) {
            Block b = new Block(initialX + i, initialY, tail);
            blocks.add(b);
            tail = b;
        }
    }

    public static int getDirection() {
        return head.direction;
    }

    public static void changeDirection(int d) {
        head.direction = d;
    }
}
