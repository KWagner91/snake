package model;

import javafx.scene.shape.Rectangle;

public class Block extends Rectangle {
    private int posX;
    private int posY;
    private int oldPosX;
    private int oldPosY;
    public Block previous;
    public static final int UP = 0;
    public static final int RIGHT = 1;
    public static final int DOWN = 2;
    public static final int LEFT = 3;
    public int direction = LEFT;
    public static int blockSize = 20;
    private int maxX;
    private int maxY;
    private Model model;

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public int getOldPosX() {
        return oldPosX;
    }

    public int getOldPosY() {
        return oldPosY;
    }

    public Block(int posX, int posY, Block previous) {
        this.posX = posX;
        this.posY = posY;

        this.previous = previous;
        this.maxX = model.WIDTH;
        this.maxY = model.HEIGHT;
    }

    public void update() {
        oldPosX = posX;
        oldPosY = posY;

        if (previous == null) {
            switch(direction) {
                case UP:
                    moveUp();
                    break;
                case DOWN:
                    moveDown();
                    break;
                case LEFT:
                    moveLeft();
                    break;
                case RIGHT:
                    moveRight();
                    break;
            }
        }
        else {
            posX = previous.oldPosX;
            posY = previous.oldPosY;
        }
        updatePosition();
    }

    public void moveUp() {
        posY--;
        if (posY < 0) {
            posY = maxY - 1;
        }
    }

    public void moveDown() {
        posY++;
        if (posY >= maxY) {
            posY = 0;
        }
    }

    public void moveLeft() {
        posX--;
        if (posX < 0) {
            posX = maxX - 1;
        }
    }

    public void moveRight() {
        posX++;
        if (posX >= maxX) {
            posX = 0;
        }
    }

    public void updatePosition() {
        setTranslateX(posX * blockSize);
        setTranslateY(posY * blockSize);
    }

}
