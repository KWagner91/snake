package model;

import javafx.scene.shape.Rectangle;

public class Food extends Rectangle {
    private int posX;
    private int posY;

    public Food(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }


}
