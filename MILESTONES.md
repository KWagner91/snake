# MILESTONES OF SNAKE GAME :snake:

## AIMS :star:
A functional version of snake, the game is played with user keyboard arrow keys. The snake grows when eating food and increases speed. When the snake bites itself, it dies.

## MILESTONES TO REACH :clipboard:
1) Set up project structure (model, Input Handler, Timer, Graphics)
2) Display static snake inside game field 
3) Snake moves through the field (and walls)
4) Movement with arrow keys is implemented
5) Food is displayed
6) The snake eats food and new food appears
7) The snake grows bigger when eating food
8) The snake dies when bitting itself

## BONUS :unlock:
* Highscore :heavy_check_mark:
* Graphics :heavy_check_mark:
* Snake increases speed 