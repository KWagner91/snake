# SNAKE GAME WITH JAVAFX

A Snake Game clone using JavaFX. The snake moves accross the game field and grows when eating food. If it accidentally bites itself, the game ends.
![Screenshot](SnakeImg.png)

## Getting Started

For this project you need to download the game files and unzip them.

### Prerequisites

What things you need to install the software and how to install them

* Java
* JavaFX
* Git
* a Java Editor (e.g IntelliJ, Eclipse)

### Installing

A step by step series of examples that tell you how to get a development env running

```
git clone https://gitlab.com/KWagner91/snake/
cd snake
```


## Built With

* [Java](https://www.oracle.com/technetwork/java/javase/downloads/index.html) - The Programming Language used
* [JavaFX](https://openjfx.io/) - Creating Java applications with a modern, hardware-accelerated user interface
* [Git](https://git-scm.com/) - Open Source Version Control System


## Authors

* **Kerstin Bonev-Wagner** - *Initial work* - [Gitlab](https://gitlab.com/KWagner91)


## Acknowledgments

* P. Frank and A. Hoffman provided an initial starter code
* [Inspiration from Rosetta Code](https://rosettacode.org/wiki/Snake/Java)